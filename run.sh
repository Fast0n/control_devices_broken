#!/bin/bash

clear
echo "Control devices broken by Fast0n"
sleep 0.7
echo ""

function recovery {
	rno

	while true; do
    read -p "Il telefono è stato rilevato? [s(si)/n(no)] " sn
    case $sn in
        [Ss]* ) rsi break;;
		[Yy]* ) rsi break;;
        [Nn]* ) rno break;;
        * ) echo "Si prega di rispondere sì o no.";;
    esac
done

}

function rsi {
	sno
	sleep 10
	while true; do
    read -p "Fatto? [s(si)/n(no)] " sn
    case $sn in
        [Ss]* ) ssi break;;
		[Yy]* ) ssi break;;
        [Nn]* ) sno break;;
        * ) echo "Si prega di rispondere sì o no.";;
    esac
done


 break
}

function ssi {
	./adb shell 'echo "persist.service.adb.enable=1" >> default.prop'
	./adb shell 'echo "persist.service.debuggable=1" >> default.prop'
	./adb shell 'echo "persist.sys.usb.config=mtp,adb" >> default.prop'
	./adb shell 'echo "persist.service.adb.enable=1" >> /system/build.prop'
	./adb shell 'echo "persist.service.debuggable=1" >> /system/build.prop'
	./adb shell 'echo "persist.sys.usb.config=mtp,adb" >> /system/build.prop'
	ssh=$(cat ~/.ssh/id_rsa.pub)
	./adb shell echo  "$var" >> file.txt
	./adb shell 'reboot'
	echo Operazione completata con successo
break
}

function sno {
	clear
	echo "Control device by adb by Fast0n"
	echo ""
	echo "Mount system && Mount data"
	echo ""
	echo "Andare su 'mounts and storage' > mount /system & mount /data"
}


function rno {
	clear
	echo "Control device by adb by Fast0n"
	echo ""
	echo "Controllo della recovery"
	./adb devices
}

function si {
	sleep 0.5
	clear
	echo "Control device by adb by Fast0n"
	echo ""
	echo "Se si sta utilizzando un Nexus spostare la levetta su Recovery con i tasti +/-"
	sleep 5
	echo "Installazione della recovery CWM"
	echo ""
	./fastboot flash recovery recovery.img
	sleep 0.5
	echo "Recovery installata con successo!"
	echo ""
	echo "Riavviare il telefono in recovery (10sec)"
	sleep 1
	echo "Riavviare il telefono in recovery (9sec)"
	sleep 1
	echo "Riavviare il telefono in recovery (8sec)"
	sleep 1
	echo "Riavviare il telefono in recovery (7sec)"
	sleep 1
	echo "Riavviare il telefono in recovery (6sec)"
		sleep 1
	echo "Riavviare il telefono in recovery (5sec)"
		sleep 1
	echo "Riavviare il telefono in recovery (4sec)"
		sleep 1
	echo "Riavviare il telefono in recovery (3sec)"
		sleep 1
	echo "Riavviare il telefono in recovery (2sec)"
		sleep 1
	echo "Riavviare il telefono in recovery (1sec)"
	recovery
	break
}

function no {
	recovery
	break
}

while true; do
    read -p "Vuoi installare la recovery? [s(si)/n(no)] " sn
    case $sn in
        [Ss]* ) si break;;
		[Yy]* ) si break;;
        [Nn]* ) no break;;
        * ) echo "Si prega di rispondere sì o no.";;
    esac
done
